import axios from 'axios';

const airtable = axios.create({
  baseURL: `https://api.postcodes.io`
});

export const getPostcodeDetails = async (lon, lat) => {
  const apiUrl = `/postcodes?lon=${lon}&lat=${lat}`;
  try {
    const response = await airtable.get(apiUrl);
    return response.data.result[0].postcode;
  } catch (error) {
    console.error('Error fetching postcode details:', error);
    throw error;
  }
};

export const getPostcodetoLocation = async (postcode) => {
    const apiUrl = `/postcodes/${postcode}`;
    try {
      const response = await airtable.get(apiUrl);
      return response.data;
    } catch (error) {
      console.error('Error fetching postcode details:', error);
      throw error;
    }
  };