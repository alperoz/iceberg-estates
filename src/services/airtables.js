import axios from 'axios';

const API_KEY = 'patBVBvscP25sEFxP.faad33f096cfd6af0edf101380e50bb30fcb3733fb56c86ced4450a31af81330';
const BASE_ID = 'appgykZBGTF92MnHu';
const contactsTableId = 'tblTSgRmykDmvOMJF';
const appointmentsTableId = 'tblHgSMxTwolE6MeC';
const agentsTableId = 'tblxHgzNIVG1tMDBu';

const airtable = axios.create({
  baseURL: `https://api.airtable.com/v0/${BASE_ID}`,
  headers: {
    'Authorization': `Bearer ${API_KEY}`,
    'Content-Type': 'application/json',
  },
});

export const getAppointments = async (fields = [], params = {}) => {
  const apiUrl = `/${appointmentsTableId}`;
  let offset = null;
  let allRecords = [];

  do {
    try {
      const response = await airtable.get(apiUrl, {
        params: {
          ...params,
          offset: offset,
          fields: fields.length > 0 ? fields.join(',') : undefined,
        },
      });

      const records = response.data.records;
      allRecords = allRecords.concat(records);

      offset = response.data.offset;
    } catch (error) {
      console.error('Error getting appointments:', error);
      break;
    }
  } while (offset);

  return allRecords;
};


export const createAppointment = async (data) => {
  const apiUrl = `/${appointmentsTableId}`;
  try {
    const response = await airtable.post(apiUrl, {
      records: [
        {
          fields: data,
        },
      ],
    });
    return response.data;
  } catch (error) {
    console.error('Error creating appointment:', error);
    throw error;
  }
};

export const updateAppointment = async (recordId, data) => {
  const apiUrl = `/${appointmentsTableId}/${recordId}`;
  try {
    const response = await airtable.patch(apiUrl, {
      fields: data,
    });
    return response.data;
  } catch (error) {
    console.error('Error updating appointment:', error);
    throw error;
  }
};

export const getAgents = async () => {
  const apiUrl = `/${agentsTableId}`;
  try {
    const response = await airtable.get(apiUrl);

    return response.data.records;
  } catch (error) {
    console.error('Error getting agents:', error);
    throw error;
  }
};

export const createContact = async (data) => {
  const apiUrl = `/${contactsTableId}`;
  var records= [data];
  try {
    const response = await airtable.post(apiUrl, {
      records
    });
    return response.data.records[0].id; 
  } catch (error) {
    console.error('Error creating contact:', error);
    throw error;
  }
};

export const updateContact = async (recordId, data) => {
  const apiUrl = `/${contactsTableId}/${recordId}`;
  try {
    const response = await airtable.patch(apiUrl, {
      fields: data,
    });
    return response.data;
  } catch (error) {
    console.error('Error updating contact:', error);
    throw error;
  }
};

export const getContacts = async () => {
  const apiUrl = `/${contactsTableId}`;
  try {
    // Filter formula ekleyerek sadece boş olmayan contact_name'leri getir
    const response = await airtable.get(apiUrl, {
      params: {
        filterByFormula: `NOT({contact_name} = '')`,
      },
    });

    return response.data.records;
  } catch (error) {
    console.error('Error getting contacts:', error);
    throw error;
  }
};



