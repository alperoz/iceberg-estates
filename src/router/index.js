import { createRouter, createWebHistory } from 'vue-router'
import Dashboard from '../views/DashboardView.vue'
import Appointments from '../views/AppointmentsView.vue'
import CreateAppointment from '../views/CreateAppointmentView.vue'

const routes = [
  {
    path: '/',
    name: 'layout',
    component: Dashboard
  },
  {
    path: '/appointments',
    name: 'appointments',
    component: Appointments
  },
  {
    path: '/create-appointment',
    name: 'create-appointment',
    component: CreateAppointment
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router

